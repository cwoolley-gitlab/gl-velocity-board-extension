# GitLab Velocity Board Extension

This is a Chrome extension to add support to GitLab boards for automatically calculating iterations based on velocity,
to provide an "emergent iterations" interface similar interface
to [Pivotal Tracker](https://www.pivotaltracker.com/features). See also ["What is Velocity"](https://www.agilealliance.org/glossary/velocity). This extension is in support of the following [GitLab OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2085).

Here is a [Video demo and explanation of the extension](https://www.youtube.com/watch?v=XcHpLhs7Fl0).

## Installing from source

- Download the [latest tagged version's zipfile](https://gitlab.com/cwoolley-gitlab/gl-velocity-board-extension/-/tags)
  - unzip it
- Or for the latest version from source, clone
  [the project repo](https://gitlab.com/cwoolley-gitlab/gl-velocity-board-extension/-/tree/main).
- In Chrome Extensions page, enable `Developer Mode`
- Click `Load unpacked` and select the root directory of the unzipped file or cloned repository.

## Using the extension

- Click the extension icon (blue bars), and enter a GitLab API token with `read_api` access, then click save.
- Go to a board for a group which has an iteration cadence configured. For example
  https://gitlab.com/groups/gitlab-org/-/boards/5283620?label_name[]=Category%3ARemote%20Development
- The board should be based only on a category label, with no other filters. For example:
  `~Category:Remote Development`.
- Ensure there is a dedicated set of scoped labels to be used with the board, not used with any other board or category,
  and containing **_ONLY_** three labels (details on why this is necessary in the
  [Why doesn't standard GitLab support this?](#why-doesnt-standard-gitlab-support-this)) section. For example:
  - `rd-workflow::unprioritized`
  - `rd-workflow::prioritized`
  - `rd-workflow::done`
- These labels must have lists defined for them on the board:
  - `*workflow::unprioritized` label - contains all unprioritized issues
  - `*workflow::prioritized` label - contains all prioritized issues
  - `*workflow::done` label - initially empty, but when `Calculate Iterations` is run,
    it will be populated with minimal information (Title, link, and velocity) for
    all issues which have been closed in the previous N iterations,
    where N is the number of iterations used to calculate the velocity.
    - NOTE: Ideally the `*workflow::done` label would automatically show all `Closed` issues
      with that label, but currently boards cannot show closed issues in a label list.
      So, as a best-effort attempt to show the iterations and issues for previous iterations,
      we automatically fill in the `*workflow::done` list with iterations and markers.
  - `Open` - This list is optional, but it can be shown as necessary to identify any issues which
    are missing a `*workflow::*` label. For example, during a Pre-Iteration Planning Meeting (Pre-IPM).
- Click the "Calculate Iterations" button in the top right corner of the board.
- The velocity will be calculated and shown at the top of the `*workflow::prioritized` list.
- Dividers will be added for current and future iterations, in the proper locations based on the velocity.
- You can edit the number of iterations to use when calculating the average velocity
- You can override the velocity to experiment with "What if?" scenarios to see where how the iterations would be
  calculated with a different velocity. Note that this means the "real" velocity and number of iterations
  to use when calculating will be ignored until the board is reset. The override value must be 1 or greater.
- You can use the `reset` button to reset everything to the original state. **_However, if you are going
  to continue dragging and ordering issues on the board, it is safer to do a full refresh, as the
  DOM manipulations done by the extension may have put the board into an invalid state._**
- If any errors occur, they will be printed to the console, and the `Calculate Iterations` button
  text will change to "❗Check Console❗". If you resolve the error, you can click the button again.
  This has the side effect of making a lot of errors show up in the extension's page - this can be
  improved in the future.
- If there are more issues in the `*workflow::prioritized` list than can be initially loaded,
  an error and alert will be shown, directing you to manually scroll to the bottom of the list
  to load all issues.
- If there are no issues found in the most recent `DEFAULT_NUM_ITERATIONS_TO_AVERAGE` completed iterations
  (currently set to `4`), then the velocity will default to the `DEFAULT_VELOCITY` value (currently set to `10`)

## Workflow overview

`*workflow::unprioritized -> *workflow::prioritized -> *workflow::done`

The issues assigned to the current iteration and `*workflow::prioritized` list must be exactly
the same, or else an error will be shown.

NOTE: In Pivotal Tracker terminology, `*workflow::unprioritized` is analogous to `Icebox`, and
`*workflow::prioritized` is analogous to `Current Iteration/Backlog`. `Done` is `Done` ;)

## Known issues

- **_Don't attempt to drag or reorder any issues after calculating iterations, even if you click reset.
  Do a hard refresh first._**
- Don't add any additional lists, such as Iteration lists, and attempt to use them to reorder issues.
  This will invalidate the ordering of the `*workflow::prioritized` list.
- Details of confidential issues will not be shown in the `*workflow:done` list.
  This is because the extension is making public non-token-authenticated GraphQL calls.
  Token support may be added in the future.
- `console.error` commands are currently used as the error reporting mechanism. This has the side effect of making
  a lot of errors show up in the extension's page.
- There's some unimplemented edge cases with the velocity calculation algorithm:
  - The unused "remainder" unallocated velocity from an iteration is not "rolled over" and added to the next iteration.
    But if you follow the good practice to only prioritize small issues (weight of `5` or less), this discrepancy
    will only be a maximum of `4` points per iteration, and usually less or zero.
  - If the first issue in the iteration has a weight greater than the velocity, multiple empty iterations should be
    created to account for the effort. But this should only matter if the velocity is very low, and again if you
    only prioritize small issues with a weight of `5` or less, this will only occur if your velocity is `5` or less.
- Things might get weird around the time the iteration rolls over (on Sundays)

## Why doesn't standard GitLab support this?

Several reasons:

- Iteration cadences don't have this velocity-based "emergent iterations" paradigm. They assume that issues are manually
  assigned to iterations. The only automation is that issues in the current iteration are automatically.
  copied to the next iteration when the current iteration ends.
- Iteration cadences have no concept of a velocity that spans iterations, it is only calculated for a single iteration.
- Board lists based on an iteration can only be defined to show a single iteration, not multiple iterations.
- Board lists based on a label can only show Open issues, so the `*workflow::done` list is always empty by default.
- Ordering and prioritization of issues in board lists can be confusing and brittle. E.g., if the relative order of two
  issues on a completely unrelated board with different lists is changed, then the order of those issues on this board
  will also change.
- Board lists can only be based on a single label. That's why we have to rely on a dedicated set of workflow labels
  which exactly match the board lists, rather than reusing existing standard `workflow::*` labels.
- Although not technically a blocker, following [this process](https://about.gitlab.com/handbook/engineering/development/dev/create/ide/#-remote-development-planning-process)
  while attempting to have small granular issues of 5 points or less ends up requiring that the team
  [enforces a 1-to-1 relationship between issues and MRs](https://about.gitlab.com/handbook/engineering/development/dev/create/ide/#1-to-1-relationship-of-issues-to-mrs),
  since MRs cannot be assigned weights or iterations, and cannot be shown on boards. A potential workaround to this would be to use
  [Tasks](https://docs.gitlab.com/ee/user/tasks.html) with a 1-to-1 relationship to MRs, but this also is not possible because
  [tasks cannot be shown on boards](https://gitlab.com/gitlab-org/gitlab/-/issues/368816#note_1598468989)

However, the good news is that there are plans to improve some of these areas. You can follow the
["Team velocity and volatility" epic](https://gitlab.com/groups/gitlab-org/-/epics/435)
for details and progress.

## Hot reloading from source in dev

- Hot reloading is only supported with the Chrome v2 extension manifest format
- Both v2 and v3 manifests are provided, and the correct one can be symlinked depending on
  whether development or production mode is desired
