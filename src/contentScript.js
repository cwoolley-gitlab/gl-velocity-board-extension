/*
 - This script uses the Railway Oriented Programming pattern for the non-async domain logic parts:
   https://fsharpforfunandprofit.com/rop/
 */

// Constants
// ---------

// Number of iterations used when calculating average velocity (total points / num iterations)
const DEFAULT_NUM_ITERATIONS_TO_AVERAGE = 4
// Initial velocity used if there are no past iterations, or reverted to if there were no issues
// closed in the last DEFAULT_NUM_ITERATIONS_TO_AVERAGE iterations (due to inactivity)
const DEFAULT_VELOCITY = 10

// Result type for use in Railway Oriented Programming
// ---------------------------------------------------

class Result {
  static ok(value) {
    return new Result(true, value, null)
  }

  static fail(error) {
    return new Result(false, null, error)
  }

  constructor(isOk, value, error) {
    this.isOk = isOk
    this.value = value
    this.error = error
  }

  match({Ok, Err}) {
    return this.isOk ? Ok(this.value) : Err(this.error)
  }

  // This is the "bind" operation for Result type
  andThen(fn) {
    if (this.isOk) {
      return fn(this.value)
    } else {
      return this
    }
  }
}

// Common helper functions
// -----------------------

const parseGroupBoardUrl = () => {
  // parse /groups/:groupFullPath/-/boards/:boardId from URL
  const urlPattern = /^\/groups\/(.+?)\/-\/boards\/(\d+)$/
  const url = new URL(window.location.href)
  const match = urlPattern.exec(url.pathname)

  const isGroupBoard = Boolean(match)
  if (!isGroupBoard) return {isGroupBoard}

  const groupFullPath = match[1]
  const boardId = match[2]

  return {isGroupBoard, groupFullPath, boardId}
}

const convertDate = dateString => {
  const [year, month, day] = dateString.split('-')
  const date = new Date(Date.UTC(year, month - 1, day, 12))
  const options = {month: 'long', day: 'numeric'}
  return new Intl.DateTimeFormat('en-US', options).format(date)
}

const nodeFromHtmlString = htmlString => new DOMParser().parseFromString(htmlString, 'text/html').body.firstChild

const resetAll = () => {
  resetOverrideVelocityField()
  disableIterationsToAverageField(false)
  resetIterationsToAverageField()
  clearExistingElements()
  toggleLoadingIndicator(false)
}

const clearExistingElements = () => {
  const elements = document.querySelectorAll('[data-id="velocity-board-injected-element"]')

  elements.forEach(element => {
    element.remove()
  })
}

const toggleLoadingIndicator = isLoading => {
  const calculateIterationsButton = document.getElementById('calculate-iterations-button')
  calculateIterationsButton.innerHTML = isLoading ?
    '<div class="gl-font-lg gl-pl-6 gl-pr-6">↻</div>' :
    'Calc. Iter.'
}

const resetOverrideVelocityField = () => {
  const overrideVelocityFieldElement = document.getElementById('override-velocity-input')
  overrideVelocityFieldElement.value = ''
}

const resetIterationsToAverageField = () => {
  const iterationsToAverageFieldElement = document.getElementById('iterations-to-average-input')
  iterationsToAverageFieldElement.value = DEFAULT_NUM_ITERATIONS_TO_AVERAGE
}

function disableIterationsToAverageField(disabled) {
  const iterationsToAverageInputElement = document.getElementById('iterations-to-average-input')
  const disabledStyle = 'gl-bg-red-50'
  disabled ?
    iterationsToAverageInputElement.classList.add(disabledStyle) :
    iterationsToAverageInputElement.classList.remove(disabledStyle)
  iterationsToAverageInputElement.disabled = disabled
}

function findPrioritizedList(lists) {
  return findBoardList(lists, 'prioritized')
}

function findDoneList(lists) {
  return findBoardList(lists, 'done')
}

function findBoardList(lists, listType) {
  return lists.nodes.find(list => {
    const regex = new RegExp(`::${listType}$`)
    return regex.test(list.title)
  })
}

const showErrorIndicator = () => {
  const calculateIterationsButton = document.getElementById('calculate-iterations-button')
  calculateIterationsButton.innerHTML = '<div>❗Check Console❗</div>'
}

const logResult = result => {
  console.log('Result:', result)
}

const logError = error => {
  console.error('\n‼️‼️‼️', error)
}

// Async GraphQL API functions
// ---------------------------

const performGraphqlQuery = async ({query, variables}) => {
  // NOTE: This re-retrieves the token on every query, but it's simpler than trying to retrieve it once and pass it
  // in from all the individual promises that call queries.
  const token = await new Promise((resolve) => {
    // noinspection JSUnresolvedReference
    chrome.storage.local.get('glvb_token', result => {
      const token = result.glvb_token
      if (token) {
        resolve(token)
      } else {
        const msg = 'Please set your GitLab API token in the extension options.'
        alert(msg)
        throw new Error(msg)
      }
    })
  })

  const windowUrlOrigin = new URL(window.location.href).origin
  const graphqlApiUrl = `${windowUrlOrigin}/api/graphql`

  return await fetch(graphqlApiUrl, {
    method: 'POST', headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token
    }, body: JSON.stringify({
      query, variables,
    })
  })
    .then(
      async response => {
        if (response.ok) {
          const responseJson = await response.json()
          if (responseJson.errors) {
            throw new Error(responseJson.errors.map(error => error.message).join('\n'))
          }
          return responseJson
        } else {
          throw new Error('Network response was not ok for GraphQL query.')
        }
      }
    )
}

// GraphQL queries
// ---------------

const boardQuery = ({groupFullPath, boardId}) => {
  const query = `
    query board(
      $groupFullPath: ID!,
      $iterationPlanningBoardId: BoardID!,
    ) {
      group(fullPath: $groupFullPath) {
        board(id: $iterationPlanningBoardId) {
          id
          name
          webUrl
          lists {
            nodes {
              id
              title
              listType
              position
            }
          }
          labels {
            nodes {
              id
              title
            }
          }
        }
      }
    }
  `
  const iterationPlanningBoardId = `gid://gitlab/Board/${boardId}`
  const variables = {
    groupFullPath, iterationPlanningBoardId,
  }
  return {query, variables}
}

const issuesForBoardListQuery = ({boardListGid, labelNames}) => {
  const query = `
    query issuesForBoardList(
      $boardListGid: ListID!,
      $labelNames: [String]
    ) {
      boardList(
        id: $boardListGid,
        issueFilters: {
          labelName: $labelNames
        },
      ) {
        id
        title
        issues(
          filters: {
            labelName: $labelNames
          },
          first: 100,
        ) {
          count
          nodes {
            id
            iid
            state
            relativePosition
            weight
            title
            webUrl
            iteration {
              id
              startDate
              dueDate
              webUrl
              iterationCadence {
                id
              }
            }
          }
          pageInfo {
            endCursor
            hasNextPage
          }
        }
      }
    }
  `
  const variables = {boardListGid, labelNames}
  return {query, variables}
}

const iterationsForGroupQuery = ({groupFullPath, iterationsCadenceGid, state, numIterations, statsType}) => {
  // NOTE: The commented parts of this query are due to excessive complexity causing query to fail
  const query = `
    query iterationsForGroup(
      $groupFullPath: ID!,
      $iterationsReportFullPath: String
      $iterationsCadenceId: IterationsCadenceID!,
      $first: Int,
      $state: IterationState
    ) {
      group(fullPath: $groupFullPath) {
        iterations(
          iterationCadenceIds: [$iterationsCadenceId],
          first: $first,
          state: $state
          sort: CADENCE_AND_DUE_DATE_DESC
        ) {
          nodes {
            id
            # iid
            startDate
            dueDate
            webUrl
            # sequence
            # state
            iterationCadence {
              id
            }
            report(fullPath: $iterationsReportFullPath) {
              error {
                code
                message
              }
              # burnupTimeSeries {
              #   date
              #   completedCount
              #   completedWeight
              #   scopeCount
              #   scopeWeight
              # }
              stats {
                ${statsType} {
                  count
                  weight
                }
              }
            }
          }
        }
      }
    }
  `
  const variables = {
    groupFullPath,
    iterationsReportFullPath: groupFullPath,
    iterationsCadenceId: iterationsCadenceGid,
    state,
    first: numIterations,
  }
  return {query, variables}
}

const iterationsIssuesQuery = ({groupFullPath, iterationIds}) => {
  const query = `
    query iterationIssues($groupFullPath: ID!, $iterationId: [ID]) {
      group(fullPath: $groupFullPath) {
        issues(
          iterationId: $iterationId
          sort: CLOSED_AT_ASC
          includeSubgroups: true
        ) {
          count
          nodes {
            id
            iid
            title
            state
            webUrl
            weight
            closedAt
            iteration {
              id
            }
          }
        }
      }
    }
  `
  const variables = {
    groupFullPath,
    iterationId: iterationIds,
  }
  return {query, variables}
}

// Render iteration data: Main entry point
// --------------------------------------

const renderIterationData = () => {
  toggleLoadingIndicator(true)
  clearExistingElements()

  try {
    const {groupFullPath, boardId} = parseGroupBoardUrl()
    const numIterationsToAverage = Number(document.getElementById('iterations-to-average-input').value)

    // NOTE: The performGraphqlQuery function is async and returns a promise, so we cannot use
    // the Railway Oriented Programming pattern with it. So, we make all necessary
    // calls to it first and resolve all the corresponding promises, and in the final `then`
    // callback we use the Railway Oriented Programming pattern within the `renderIterationCalculateAndDisplay`
    // function which contains all the non-async domain logic.
    const boardQueryPromise = performGraphqlQuery(boardQuery({groupFullPath, boardId}))
    const issuesForBoardListQueryPromise = boardQueryPromise
      .then(
        boardQueryResponse => {
          const {board} = boardQueryResponse.data.group
          const boardLabels = board.labels.nodes.map(labelNode => labelNode.title)
          const {lists} = board
          const prioritizedList = findPrioritizedList(lists)
          if (!prioritizedList) {
            throw new Error('Unable to find board list for scoped label ending in "::prioritized".\n' +
              'This list must exist and contain all prioritized issues in priority order.')
          }
          return performGraphqlQuery(issuesForBoardListQuery({
            boardListGid: prioritizedList.id,
            labelNames: boardLabels
          }))
        }
      )
    const currentIterationForGroupQueryPromise = issuesForBoardListQueryPromise
      .then(issuesForBoardListQueryResponse => {
        validatePrioritizedIssues({issuesForBoardListQueryResponse})
        const {
          data: {
            boardList: {
              issues: {
                nodes: {
                  0: {
                    iteration: {
                      iterationCadence: {
                        id: iterationsCadenceGid
                      }
                    }
                  }
                }
              }
            }
          }
        } = issuesForBoardListQueryResponse
        return performGraphqlQuery(iterationsForGroupQuery({
          groupFullPath,
          iterationsCadenceGid,
          state: 'current',
          numIterations: 1,
          statsType: 'total',
        }))
      })
    const closedIterationsForGroupQueryPromise = currentIterationForGroupQueryPromise
      .then(closedIterationsForGroupQueryResponse => {
        const {
          data: {
            group: {
              iterations: {
                nodes: {
                  0: {
                    iterationCadence: {
                      id: iterationsCadenceGid
                    }
                  }
                }
              }
            }
          }
        } = closedIterationsForGroupQueryResponse
        return performGraphqlQuery(iterationsForGroupQuery({
          groupFullPath,
          iterationsCadenceGid,
          state: 'closed',
          numIterations: numIterationsToAverage,
          statsType: 'complete',
        }))
      })
    // Note that closedIterationsIssuesQueryPromise is chained off of closedIterationsForGroupQueryPromise,
    // because it needs the closed iterations
    const closedIterationsIssuesQueryPromise = closedIterationsForGroupQueryPromise
      .then(closedIterationsForGroupQueryResponse => {
        const {
          data: {
            group: {
              iterations: {
                nodes: iterations
              }
            }
          }
        } = closedIterationsForGroupQueryResponse
        const iterationIds = iterations.map(iteration => iteration.id)
        return performGraphqlQuery(iterationsIssuesQuery({
          groupFullPath,
          iterationIds,
        }))
      })
    // Note that currentIterationIssuesQueryPromise is chained off of currentIterationForGroupQueryPromise,
    // because it needs the closed iterations
    const currentIterationIssuesQueryPromise = currentIterationForGroupQueryPromise
      .then(currentIterationForGroupQueryResponse => {
        const {
          data: {
            group: {
              iterations: {
                nodes: iterations
              }
            }
          }
        } = currentIterationForGroupQueryResponse
        const iterationIds = iterations[0].id
        return performGraphqlQuery(iterationsIssuesQuery({
          groupFullPath,
          iterationIds,
        }))
      })

    Promise.all(
      [
        boardQueryPromise,
        issuesForBoardListQueryPromise,
        closedIterationsForGroupQueryPromise,
        currentIterationForGroupQueryPromise,
        closedIterationsIssuesQueryPromise,
        currentIterationIssuesQueryPromise,
      ])
      .then(
        ([
           boardQueryResponse,
           issuesForBoardListQueryResponse,
           closedIterationsForGroupQueryResponse,
           currentIterationForGroupQueryResponse,
           closedIterationsIssuesQueryResponse,
           currentIterationIssuesQueryResponse,
         ]) => {
          renderIterationCalculateAndDisplay({
            boardQueryResponse,
            issuesForBoardListQueryResponse,
            closedIterationsForGroupQueryResponse,
            currentIterationForGroupQueryResponse,
            closedIterationsIssuesQueryResponse,
            currentIterationIssuesQueryResponse,
          })
          toggleLoadingIndicator(false)
        })
      .catch(error => {
        logError(error)
        clearExistingElements()
        showErrorIndicator()
        // TODO: Why isn't this caught by the outer try/catch? Because of the Promise?
        // throw error
      })
  } catch (error) {
    logError(error)
    clearExistingElements()
    showErrorIndicator()
  }
}

// Render iteration data: Validate prioritized issues
// --------------------------------------------------

const validatePrioritizedIssues = (pipeData) => {
  validatePrioritizedListContainsAtLeastOneIssue(pipeData)
    .andThen(validatePrioritizedIssuesAreAllAssignedToAnIteration)
    .andThen(validatePrioritizedIssueAreAllAssignedToCurrentIteration)
    .andThen(validatePrioritizedIssuesAreAllLoaded)
    .match({
      Ok: (_) => {
      },
      Err: error => {
        throw error
      }
    })
}

function validatePrioritizedListContainsAtLeastOneIssue(pipeData) {
  // Validate that there is at least 1 issue in the prioritized list

  const {
    issuesForBoardListQueryResponse: {
      data: {
        boardList: {
          issues: {
            nodes: prioritizedIssues,
          },
          title: boardListTitle,
        }
      }
    }
  } = pipeData

  if (!prioritizedIssues.length) {
    return Result.fail(`No issues found for scoped label ending in "${boardListTitle}". This list must contain at least 1 issue.`)
  }

  return Result.ok(pipeData)
}

function validatePrioritizedIssuesAreAllAssignedToAnIteration(pipeData) {
  // Validate that all issues in the prioritized list are assigned to an iteration

  const {
    issuesForBoardListQueryResponse: {
      data: {
        boardList: {
          issues: {
            nodes: prioritizedIssues,
          },
          title: boardListTitle,
        }
      }
    }
  } = pipeData

  for (let {iteration, title, webUrl} of prioritizedIssues) {
    if (!iteration) {
      return Result.fail(`No iteration was assigned for issue ${webUrl} ("${title}").\n` +
        `All issues in the "${boardListTitle}" list must be assigned to an iteration.`)
    }
  }
  return Result.ok(pipeData)
}

// TODO: Rewrite this to use currentIterationForGroupQueryResponse instead of manually checking dates
function validatePrioritizedIssueAreAllAssignedToCurrentIteration(pipeData) {
  // Validate that all issues in the prioritized list are assigned to the current iteration

  const {
    issuesForBoardListQueryResponse: {
      data: {
        boardList: {
          issues: {
            nodes: prioritizedIssues,
          },
          title: boardListTitle,
        }
      }
    }
  } = pipeData

  for (let {iteration, title, webUrl} of prioritizedIssues) {
    const startDateString = iteration.startDate
    const startDateUtc = new Date(startDateString + 'T00:00:00Z')
    const dueDateString = iteration.dueDate
    const dueDateUtc = new Date(dueDateString + 'T00:00:00Z')
    const now = new Date()
    const nowUtc = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds()))
    const issueIsInCurrentIteration = startDateUtc <= nowUtc && nowUtc <= dueDateUtc
    if (!issueIsInCurrentIteration) {
      return Result.fail(`Issue is not in current iteration: ${webUrl} ("${title}").\n` +
        `All issues in the "${boardListTitle}" list must be assigned to the current iteration (not a past or future iteration).\n` +
        `Issue iteration info: ${startDateString} - ${dueDateString}, ${iteration.webUrl}.\n` +
        `Current time in UTC = ${nowUtc}\n` +
        `Issue iteration dates in UTC: startDateUtc: ${startDateUtc}, dueDateUtc: ${dueDateUtc}\n` +
        `!!!!NOTE!!!!: There may be timezone issues with this check, but unfortunately the graphql API for iteration ` +
        `start/due date only returns a date string, not a timestamp, and provides no way to directly retrieve the ` +
        `current iteration for a cadence.`)
    }
  }

  return Result.ok(pipeData)
}

function validatePrioritizedIssuesAreAllLoaded(pipeData) {
  // Validate that all issues in the displayed priority list are loaded and rendered
  // Note that we have to detect this via various means, as the UI can be in different states depending on the loaded
  // state, and size/scaling of the window

  const {
    issuesForBoardListQueryResponse: {
      data: {
        boardList: {
          id: boardListGid,
          title: boardListTitle,
        }
      }
    }
  } = pipeData

  // If there is no li.board-list-count element, then all issues are loaded
  const boardListCountElement = document.querySelector(`[data-list-id="${boardListGid}"] li.board-list-count`)
  if (!boardListCountElement) return Result.ok(pipeData)

  /// if 'Showing all issues' is displayed, then all issues are loaded
  const boardListCountSpanElement = boardListCountElement.querySelector('span')
  const allPrioritizedIssuesLoaded = boardListCountSpanElement.textContent === 'Showing all issues'
  if (allPrioritizedIssuesLoaded) return Result.ok(pipeData)

  // Otherwise all issues are not yet loaded
  const errMsg = `The "${boardListTitle}" list has not yet loaded all issues. Please manually scroll to the bottom of the list and try again.`
  alert(`⚠️ ${errMsg}`)
  return Result.fail(errMsg)
}

// Render iteration data: Calculate and display
// --------------------------------------------

const renderIterationCalculateAndDisplay = rawQueryResponses => {
  initializePipeData(rawQueryResponses)
    .andThen(sanityChecks)
    .andThen(validateCurrentIterationIssuesAreAllPrioritized)
    .andThen(calculateClosedIterationDataAndVelocity)
    .andThen(calculateCurrentAndFutureIterationData)
    .andThen(addVelocityToPrioritizedListHeader)
    .andThen(addCurrentAndFutureIterationDividers)
    .andThen(addDoneIterationElements)
    .andThen(formatResult)
    .match({
      Ok: logResult,
      Err: error => {
        throw error
      }
    })
}

function initializePipeData(rawQueryResponses) {
  return Result.ok(rawQueryResponses)
}

function sanityChecks(pipeData) {
  // Various sanity checks that data is consistent. If any of these fail, there's a bug somewhere, either in
  // this script, or in GitLab - maybe some incorrect caching going on...

  const {
    currentIterationForGroupQueryResponse: {
      data: {
        group: {
          iterations: {
            nodes: {
              0: {
                report: {
                  stats: {
                    total: {
                      count: currentIterationIssueCountFromIterationsReport,
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    currentIterationIssuesQueryResponse: {
      data: {
        group: {
          issues: {
            count: currentIterationIssueCount,
          }
        }
      }
    }
  } = pipeData

  // Sanity check that these two counts from the different queries match.
  if (currentIterationIssueCountFromIterationsReport !== currentIterationIssueCount) {
    return Result.fail(
      'currentIterationIssueCountFromIterationsReport !== currentIterationIssueCount - \n' +
      'this is likely because there are confidential issues assigned to the current iteration \n' +
      'which you do not have access to. Please go to the view the current iteration in the UI, \n' +
      `and verify that you can see all ${currentIterationIssueCountFromIterationsReport} issues.`
    )
  }

  return Result.ok(pipeData)
}

function validateCurrentIterationIssuesAreAllPrioritized(pipeData) {
  // Validate that the issues in the displayed priority list represent all issues assigned to the current iteration.

  // Get the current iteration GID from the first issue in the prioritized list. We can trust that this
  // will be the current iteration, because we've previously validated that there is at least one issue
  // in the prioritized list, and that all issues in the prioritized list are assigned to the current iteration3
  const {
    issuesForBoardListQueryResponse: {
      data: {
        boardList: {
          issues: {
            count: prioritizedOpenIssuesCount,
          }
        }
      }
    },
    currentIterationForGroupQueryResponse: {
      data: {
        group: {
          iterations: {
            nodes: {
              0: {
                webUrl,
              }
            }
          }
        }
      }
    },
    currentIterationIssuesQueryResponse: {
      data: {
        group: {
          issues: {
            count: currentIterationIssueCount,
            nodes: currentIterationIssues,
          }
        }
      }
    }
  } = pipeData

  const issuesByState = currentIterationIssues.reduce((acc, issue) => {
    const {state} = issue
    acc[state].push(issue)
    return acc
  }, {opened: [], closed: [], locked: []})
  const prioritizedClosedIssuesCount = issuesByState.closed.length

  const prioritizedTotalIssuesCount = prioritizedOpenIssuesCount + prioritizedClosedIssuesCount
  if (prioritizedTotalIssuesCount !== currentIterationIssueCount) {
    return Result.fail(
      `The issues in the prioritized list (${prioritizedTotalIssuesCount}}) ` +
      `do not represent all issues assigned to the current iteration (${currentIterationIssueCount}}).\n` +
      `Please ensure that all issues assigned to the current iteration are in the prioritized list, or remove them from the current iteration\n` +
      `However, DO NOT do it by dragging them to or from a board iteration list, as this can mess up the priority!\n` +
      `Instead, do it for each issue individually or via an issues bulk edit.\n` +
      `Here is the URL to the current iteration and its issues for convenience:\n` +
      `${webUrl}.\n`
    )
  }

  return Result.ok(pipeData)
}

const calculateClosedIterationDataAndVelocity = pipeData => {
  const {
    closedIterationsForGroupQueryResponse,
  } = pipeData
  const closedIterations = closedIterationsForGroupQueryResponse.data.group.iterations.nodes

  let completedWeights = 0
  let completedCount = 0
  const closedIterationSummaries = []

  closedIterations.forEach(iteration => {
    const {stats} = iteration.report
    const statsForCompleted = stats ? stats.complete : {count: 0, weight: 0}
    completedWeights += statsForCompleted.weight
    completedCount += statsForCompleted.count
    const {startDate, dueDate, webUrl, id} = iteration
    closedIterationSummaries.push({startDate, dueDate, webUrl, id, ...statsForCompleted})
  })

  // If issues were completed in the closed iterations
  const calculatedVelocity = completedCount ?
    // calculate the velocity based on the average, with a minimum of 1 (0 is not allowed)  
    Math.round(completedWeights / closedIterations.length) || 1 :
    // otherwise use the initial/default velocity
    DEFAULT_VELOCITY

  const velocityOverride = document.getElementById('override-velocity-input')?.value
  if (velocityOverride === '0') {
    return Result.fail('Velocity override must be greater than 0')
  }

  const velocity = velocityOverride ? Number(velocityOverride) : calculatedVelocity

  pipeData = {
    closedIterationSummaries,
    velocity,
    closedIterations,
    ...pipeData,
  }
  return Result.ok(pipeData)
}

function calculateCurrentAndFutureIterationData(pipeData) {
  const {
    closedIterationSummaries,
    velocity,
    issuesForBoardListQueryResponse,
    currentIterationIssuesQueryResponse
  } = pipeData
  const {
    data: {
      boardList: {
        issues: {
          count: prioritizedListTotalCount,
          pageInfo,
          nodes: prioritizedListIssues,
        }
      }
    }
  } = issuesForBoardListQueryResponse

  // Check that all issues in the prioritized list were loaded.
  const {hasNextPage} = pageInfo
  if (hasNextPage) {
    return Result.fail(
      `There were only ${prioritizedListIssues.length} issues loaded in the '~*workflow::prioritized' ` +
      `list, out of ${prioritizedListTotalCount}. Cannot continue calculating iteration data, because this extension ` +
      'does not yet support paginating GraphQL response cursors.')
  }

  // get the issues from the current iteration to get closed issues - the board list only contains open issues
  const {
    data: {
      group: {
        issues: {
          nodes: currentIterationIssues,
        }
      }
    }
  } = currentIterationIssuesQueryResponse

  // NOTE: There may be an edge case here if an issue is closed without ever being assigned to
  //       the prioritized label, nor ever being assigned to the current iteration. Hopefully
  //       triage bot automation will prevent this from happening

  // Filter out closed issues and sort them by closedAt ascending
  const prioritizedClosedIssues = currentIterationIssues
    .filter(issue => issue.state === 'closed')
    .sort(({closedAt: a}, {closedAt: b}) => a - b)

  // calculate the current iteration's start and due dates one week after the last closed iteration
  const iterationStartDate = new Date(closedIterationSummaries[0].startDate)
  iterationStartDate.setDate(iterationStartDate.getDate() + 7)
  const iterationDueDate = new Date(closedIterationSummaries[0].dueDate)
  iterationDueDate.setDate(iterationDueDate.getDate() + 7)

  // currentAndFutureIterations is the array of iteration objects that we are populating
  const currentAndFutureIterations = []

  // accumulator variables for each iteration loop
  let iterationIssues = []
  let iterationWeight = 0

  // sort the prioritized list issues by relativePosition
  const prioritizedOpenIssues = prioritizedListIssues
    .sort(({relativePosition: a}, {relativePosition: b}) => a - b)

  // combine the closed and open issues into one array, with the closed issues first, sorted by ascending closedAt
  const prioritizedIssues = [...prioritizedClosedIssues, ...prioritizedOpenIssues]

  // populate the currentAndFutureIterations array based on the prioritizedIssues
  prioritizedIssues.forEach(issue => {
    const issueWeight = issue.weight
    const issueWeightSpecified = issueWeight !== null && issueWeight !== undefined

    // if the current issue is NOT closed (all closed issues should be in the first iteration), 
    // AND it has caused the total to exceed the velocity, create a new emergent iteration with all
    // the previous issues which have already fit into it without exceeding the velocity. The current issue will
    // be the first issue in the next iteration
    const shouldStartNewIteration =
      issue.state !== 'closed' &&
      issueWeightSpecified && iterationWeight + issueWeight > velocity
    if (shouldStartNewIteration) {
      // TODO: Handle edge case where a single issue weight is greater than the velocity, and we need to
      //       create multiple empty iterations until we have "allocated" enough velocity to hold the issue.
      currentAndFutureIterations.push({
        startDate: iterationStartDate.toISOString().slice(0, 10),
        dueDate: iterationDueDate.toISOString().slice(0, 10),
        issues: iterationIssues,
        count: iterationIssues.length,
        weight: iterationWeight
      })

      iterationStartDate.setDate(iterationStartDate.getDate() + 7)
      iterationDueDate.setDate(iterationDueDate.getDate() + 7)
      iterationIssues = []
      iterationWeight = 0
    }

    iterationIssues.push(issue)
    if (issueWeightSpecified) iterationWeight += issueWeight
  })

  // Create the last iteration, if we still have any issues left
  if (iterationIssues.length > 0) {
    currentAndFutureIterations.push({
      startDate: iterationStartDate.toISOString().slice(0, 10),
      dueDate: iterationDueDate.toISOString().slice(0, 10),
      issues: iterationIssues,
      count: iterationIssues.length,
      weight: iterationWeight
    })
  }

  return Result.ok({...pipeData, currentAndFutureIterations})
}

const addVelocityToPrioritizedListHeader = pipeData => {
  const {velocity, boardQueryResponse: {data: {group: {board: {lists}}}}} = pipeData
  const prioritizedListGid = findPrioritizedList(lists).id

  // Find the label element in the 'prioritized' list
  const prioritizedBoardListTitleElement =
    document.querySelector(`[data-list-id="${prioritizedListGid}"] .board-title-text`)

  prioritizedBoardListTitleElement.after(velocityNode(velocity))

  return Result.ok(pipeData)
}

function velocityNode(velocity) {
  const htmlString = `<div class="gl-mr-2 gl-font-sm gl-display-inline-flex gl-pr-2 no-drag gl-text-secondary" ` +
    ` data-id="velocity-board-injected-element" title="Velocity">` +
    `<span class="gl-font-lg gl-display-inline-flex">` + `📶 ${velocity}` +
    `</span></div>`
  return nodeFromHtmlString(htmlString)
}

const addCurrentAndFutureIterationDividers = pipeData => {
  const {currentAndFutureIterations} = pipeData

  function findFirstOpenedIssue(issues) {
    return issues.find(issue => issue.state === 'opened')
  }

  const firstIterationWithOpenIssues = currentAndFutureIterations.find(iteration => {
    return iteration.issues.length > 0 && findFirstOpenedIssue(iteration.issues)
  })

  for (let iteration of currentAndFutureIterations) {
    const firstIssueGid = (findFirstOpenedIssue(iteration.issues) || findFirstOpenedIssue(firstIterationWithOpenIssues.issues)).id
    const issueSelector = `[data-item-id="${firstIssueGid}"]`
    const element = document.querySelector(issueSelector)

    if (!element) return Result.fail(
      `Could not find element to insert iteration dividers after for issueSelector: '${issueSelector}'`
    )

    const iterationDividerData = {
      startDate: convertDate(iteration.startDate),
      dueDate: convertDate(iteration.dueDate),
      count: iteration.count,
      weight: iteration.weight,
      closedCount: iteration.issues.filter(issue => issue.state === 'closed').length,
    }
    element.before(iterationDividerNode(iterationDividerData))

    // Manually add any closed issues to the prioritized list, because label lists on boards never show closed issues.
    // This will only happen for the current (first) iteration, by definition future iterations can't contain closed issues.
    const closedIssues = iteration.issues.filter(issue => issue.state === 'closed')
    for (let closedIssue of closedIssues) {
      closedIssue['title'] = `${closedIssue.title} <em>(Closed)</em>`
      element.before(issueNode(closedIssue))
    }
  }

  return Result.ok(pipeData)
}

const addDoneIterationElements = pipeData => {
  const {
    closedIterationSummaries,
    closedIterationsIssuesQueryResponse,
    boardQueryResponse: {data: {group: {board: {lists}}}}
  } = pipeData
  const doneListGid = findDoneList(lists).id

  // Find the label element in the 'prioritized' list
  const doneListBoardUlElement = document.querySelector(`ul[data-board="${doneListGid}"]`)

  const {
    data: {
      group: {
        issues: {
          nodes: issues,
        }
      }
    }
  } = closedIterationsIssuesQueryResponse

  const issuesByIterationGid = issues.reduce((acc, issue) => {
    const {iteration: {id}} = issue
    if (!acc[id]) {
      acc[id] = []
    }
    acc[id].push(issue)
    return acc
  }, {})

  for (let iterationSummary of [...closedIterationSummaries].reverse()) {
    const iterationDividerData = {
      startDate: convertDate(iterationSummary.startDate),
      dueDate: convertDate(iterationSummary.dueDate),
      count: iterationSummary.count,
      weight: iterationSummary.weight
    }

    doneListBoardUlElement.appendChild(iterationDividerNode(iterationDividerData))

    const issuesForIteration = issuesByIterationGid[iterationSummary.id]

    if (!issuesForIteration) continue

    for (let issue of issuesForIteration) {
      doneListBoardUlElement.appendChild(issueNode(issue))
    }

    const confidentialIssueCount = iterationDividerData.count - issuesForIteration.length
    if (confidentialIssueCount > 0) {
      for (let i = 0; i < confidentialIssueCount; i++) {
        const issue = {
          title: '<del>Confidential Issue</del>',
          webUrl: 'https://confidential-gitlab-issue.example.com',
          weight: '?',
        }
        doneListBoardUlElement.appendChild(issueNode(issue))
      }
    }
  }

  return Result.ok(pipeData)
}

function iterationDividerNode({startDate, dueDate, count, weight, closedCount}) {
  const htmlString =
    `<li data-id="velocity-board-injected-element">` +
    `<div class="gl-p-2 gl-rounded-base gl-line-height-normal gl-relative gl-mb-3 gl-display-flex gl-justify-content-space-between"` +
    ` data-id="velocity-board-injected-element" style="background-color: grey">` +
    `<div class="gl-font-lg">` +
    `<span title="Unfortunately, this is just an emoji arrow, it doesn't expand or collapse 😢">⏷</span> ` +
    `${weight} points, ${count} ${count === 1 ? 'issue' : 'issues'}` +
    `${closedCount ? ' (' + closedCount + ' closed)' : ''}` +
    `</div>` +
    `<div>${startDate} - ${dueDate}</div>` +
    `</div>` +
    `</li>`
  return nodeFromHtmlString(htmlString)
}

function issueNode(issue) {
  const htmlString =
    `<li data-id="velocity-board-injected-element"` +
    ` class="board-card gl-pl-5 gl-pr-3 gl-py-2 gl-rounded-base gl-line-height-normal gl-relative gl-mb-3">` +
    `<div class="gl-display-flex gl-flex-direction-row gl-justify-content-space-between">` +
    `<div dir="auto" class="gl-display-flex">` +
    `<h4 class="board-card-title gl-min-w-0 gl-mb-0 gl-mt-0 gl-font-base gl-overflow-wrap-anywhere">` +
    `<a href="${issue.webUrl}" class="js-no-trigger gl-text-body gl-hover-text-gray-900">${issue.title}</a>` +
    `</h4>` +
    `</div>` +
    `<a class="board-card-info board-card-weight gl-cursor-pointer gl-text-gray-500 gl-font-sm" title="weight">` +
    `<span class="board-card-info-text">${issue.weight}</span>` +
    `</a>` +
    `</div>` +
    `</li>`
  return nodeFromHtmlString(htmlString)
}

const formatResult = pipeData => {
  // Print out pipeData for debugging / auditing use.
  return Result.ok({message: '🎉Successfully rendered velocity and iterations.', data: pipeData})
}

// Content script entry point
// --------------------------

const contentScript = () => {
  initialize()
}

// Content script initialization
// -----------------------------

const initialize = () => {
  const {isGroupBoard} = parseGroupBoardUrl()

  if (!isGroupBoard) return

  const intervalId = setInterval(() => {
    const createListButton = document.querySelector('[data-testid="boards-create-list"]')

    // wait for next interval if the board is not yet rendered
    if (!createListButton) return

    // wait for next interval if we have already added the Calculate Iterations button
    if (createListButton.dataset.calculateIterationsButtonAdded) return

    const calculateIterationsButton = injectCalculateIterationsButton(createListButton)
    const resetAllButton = injectResetAllButton(calculateIterationsButton)
    const overrideVelocityField = injectOverrideVelocityField(resetAllButton)
    injectIterationsToAverageField(overrideVelocityField)

    clearInterval(intervalId)
  }, 500)
}

// Content script initialization: UI controls
// ------------------------------------------

const injectCalculateIterationsButton = createListButton => {
  createListButton.dataset.calculateIterationsButtonAdded = 'true'
  const calculateIterationsButton = calculateIterationsButtonNode()
  calculateIterationsButton.addEventListener('click', renderIterationData)

  createListButton.after(calculateIterationsButton)
  return calculateIterationsButton
}

const calculateIterationsButtonNode = () => {
  const htmlString = '<button type="button" class="btn btn-confirm btn-md gl-button gl-ml-3">' +
    '<span title="CALCULATE ITERATIONS: Calculate iterations for prioritized and done lists based on velocity"' +
    'id="calculate-iterations-button" class="gl-button-text">Calc. Iter.</span>' +
    '</button>'
  return nodeFromHtmlString(htmlString)
}

const injectResetAllButton = calculateIterationsButton => {
  const resetAllButton = resetAllButtonNode()
  resetAllButton.addEventListener('click', resetAll)

  calculateIterationsButton.after(resetAllButton)
  return resetAllButton
}

const resetAllButtonNode = () => {
  const htmlString = '<button type="button" class="btn btn-confirm btn-md gl-button gl-ml-3">' +
    '<span title="Clear existing iteration and velocity elements and reset velocity override and iterations to average" ' +
    'id="reset-all-button" class="gl-font-lg gl-button-text">⃠</span>' +
    '</button>'
  return nodeFromHtmlString(htmlString)
}

const injectOverrideVelocityField = calculateIterationsButton => {
  const overrideVelocityField = overrideVelocityFieldNode()
  calculateIterationsButton.after(overrideVelocityField)

  const overrideVelocityInput = overrideVelocityField.querySelector('input')
  overrideVelocityInput.addEventListener('input', () => {
    if (overrideVelocityInput.value) {
      disableIterationsToAverageField(true)
    } else {
      disableIterationsToAverageField(false)
    }
  })

  return overrideVelocityField
}

const overrideVelocityFieldNode = () => {
  const htmlString =
    '<div class="gl-ml-3 gl-md-display-flex gl-md-display-flex gl-align-items-center" ' +
    ' title="VELOCITY OVERRIDE: This velocity override field allows you to experiment with different velocities than the actual calculated ' +
    'velocity. Enter a nonzero number to override calculated velocity when calculating iterations. Note that this ' +
    'makes the iterations to average field irrelevant, other than controlling the number of done iterations shown.">' +
    '<span class="gl-white-space-nowrap gl-font-weight-bold gl-line-height-normal">Velocity<br/>override:</span>' +
    '<input id="override-velocity-input" type="text"' +
    ' class="gl-ml-3 gl-p-2 gl-w-7 gl-text-center">' +
    '</div>'
  return nodeFromHtmlString(htmlString)
}

const injectIterationsToAverageField = overrideVelocityField => {
  const iterationsToAverageField = iterationsToAverageFieldNode()
  overrideVelocityField.after(iterationsToAverageField)
  return iterationsToAverageField
}

const iterationsToAverageFieldNode = () => {
  const htmlString =
    '<div class="gl-ml-3 gl-md-display-flex gl-md-display-flex gl-align-items-center" ' +
    ' title="ITERATIONS TO AVERAGE: Number of completed iterations used when calculating average velocity">' +
    '<span class="gl-white-space-nowrap gl-font-weight-bold gl-line-height-normal">Iterations<br/>to average:</span>' +
    `<input id="iterations-to-average-input" type="text" value="${DEFAULT_NUM_ITERATIONS_TO_AVERAGE}"` +
    ' class="gl-ml-3 gl-p-2 gl-w-7 gl-text-center">' +
    '</div>'
  return nodeFromHtmlString(htmlString)
}

// Invoke content script
// ---------------------

contentScript()
